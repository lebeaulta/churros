# @churros/sync

## 1.71.1

### Patch Changes

- 60e9ac7: chore(changesets): version db and fix sync / api / app packages dependencies
- c8581d2: upgrade tsx and npm-run-all2
- 88b63d1: chore(deps): bump ldap7 to v1.0.9
- Updated dependencies [60e9ac7]
- Updated dependencies [b34b81a]
  - @churros/db@1.0.0
